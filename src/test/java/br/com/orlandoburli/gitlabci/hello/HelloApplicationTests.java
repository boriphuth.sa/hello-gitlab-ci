package br.com.orlandoburli.gitlabci.hello;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloApplicationTests {

	@Autowired
	HelloApplication app;

	@Test
	public void testHelloApplicationGet() {
		assertThat("Hello, Orlando!", equalTo(this.app.hello("Orlando")));
	}

}
